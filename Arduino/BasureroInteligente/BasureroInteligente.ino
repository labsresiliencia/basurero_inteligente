#include <HCSR04.h>
#include <NeoSWSerial.h>

const int notificacion = 60000; // Notifica cada minuto
const int duracion_tono = 10000; // Duración de la notificación
const int freq_tono = 2093;
const int btn1 = 12;
const int bocina = 10;

NeoSWSerial nss( 4, 5 );
HCSR04 hc(3,2);

void setup() {
  nss.begin();
  pinMode(btn1, INPUT_PULLUP);
}

void loop() {
  // Transmite cada vez que se llega a la distancia de llenado y
  // el sensor de peso determina que no hay contenido en el basurero
  if( hc.dist() < 200 && (digitalRead(btn1)==HIGH) ) {
    nss.write('f');
    tone(bocina, freq_tono, duracion_tono);
  }

  delay(notificacion); // Espera antes de siguiente transmisión
}
